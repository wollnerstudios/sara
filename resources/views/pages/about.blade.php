@extends('layouts.default', ['navbar' => true, 'pageTitle' => 'About'])

@section('content')
    <div class="page page-about">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 copy-container">
                    <p>
                        Scientific Applications & Research Associates (SARA), Inc., was formed in 1989 to harness the
                        creativity, innovation and entrepreneur spirit of engineers and scientists. We are a family of
                        innovation pioneers. Thought leaders. Chance takers. Greatness grabbers. To put it simply, SARA
                        creates what others only imagine. Our uniquely-SARA solutions are developed for defense abroad as
                        well as at home. We develop novel technology in EMP, pulse power, and RSTA sensing that WORKS for
                        your applications. We exceed your expectations and bring more value to your budget. All this done within a
                        customer-centric culture that makes you feel less like a client and more like a partner.
                    </p>
                </div>

                <div class="col-sm-9 col-sm-offset-3">
                    <img class="content-image" src="/images/helicopter-panorama.jpg" alt="">
                </div>

                <div class="col-sm-9 copy-container">
                    <h2 class="heading">Mission</h2>

                    <p>
                        SARA is dedicated to making the impossible possible through creative and innovative research in
                        order to develop unique technologies and transform them into products and services for clients in
                        defense, homeland security, and the private sector.
                    </p>
                </div>

                <div class="col-sm-9">
                    <img src="/images/aircraft-underside.jpg" alt="" class="pull-right content-image">
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
