@extends('layouts.default', ['navbar' => true, 'pageTitle' => 'SeaPort-e'])

@section('content')
    <div class="page page-seaport">
        <div class="container">
            <table class="table">
                <tr>
                    <td>Contract Number</td>
                    <td>N00178-16-D-8998</td>
                </tr>
                <tr>
                    <td>Period of Performance</td>
                    <td>05/30/2016-09/30/2019</td>
                </tr>
                <tr>
                    <td>Contract Ceiling</td>
                    <td>$3.96 Billion (Base Period)</td>
                </tr>
                <tr>
                    <td>Operating Zones</td>
                    <td>
                        1 (Northeast), 2 (Capital), 4 (Gulf Coast), 6 (Southwest)
                        <br>
                        <br>
                        <img src="/images/seaPort_map.png" alt="" class="img-responsive content-image">
                    </td>
                </tr>
                <tr>
                    <td>Functional Areas</td>
                    <td>
                        <ul>
                            <li>Research and Development</li>
                            <li>Engineering, Systems Engineering and Process Engineering</li>
                            <li>Modeling, Simulation, Stimulation and Analysis</li>
                            <li>Prototyping, Pre-Production, Model Making and Fabrication</li>
                            <li>System Design Documentation and Technical Data</li>
                            <li>Software Engineering, Development, Programming</li>
                            <li>Interoperability, Test and Evaluation</li>
                            <li>Measurement Facilities, Range and Instrumentation</li>
                            <li>Technical Training</li>
                            <li>In-Service Engineering, Fleet Introduction, Installation & Checkout</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>Technical</td>
                    <td>
                        <p>Greg Loboda</p>

                        <p>
                            <a href="mailto:globoda@sara.com?Subject=SeaPort-e" target="_top">eMail</a>
                        </p>

                        <p>
                            6300 Gateway Drive <br>
                            Cypress, CA 90630 <br>
                            714-224-4410
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>Contracts & Customer Satisfaction</td>
                    <td>
                        <p>Ruth Craig</p>

                        <p><a href="mailto:rcraig@sara.com?Subject=SeaPort-e" target="_top">eMail</a></p>

                        <p>
                            6300 Gateway Drive <br>
                            Cypress, CA 90630 <br>
                            714-224-4410
                        </p>
                    </td>
                </tr>
            </table>

            <h1 class="heading">Quality Assurance</h1>

            <p>
                SARA’s Quality Assurance Program uses a powerful approach that has been applied successfully to numerous
                projects equivalent in size, complexity, and scope to the SeaPort-e contract. Principles of our QA
                Program include clear communications with the customer, the precise and timely application of
                performance metrics, and rigorous compliance with SARA best practices and standards. Our International
                Standards Organization (ISO) based program and compliance establishes our Quality Assurance (QA)
                infrastructure and defines the processes needed to implement it effectively. We apply the results of our
                QA activities and measurements to quantitatively assess all aspects of our performance as well as
                product and service&nbsp;quality.
            </p>

            <br>
            <br>

            <img src="/images/seaPort_graphics.png" alt="" class="img-responsive content-image" style="margin: 0 auto;width: 60%;">

        </div>
    </div>
@endsection