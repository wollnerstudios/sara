@extends('layouts.default', ['noFooter' => true])

@section('content')
    <div class="page page-home">
        <div class="container-fluid">
            <a href="#" id="menu-button" role="button">
                <img src="/images/menu-icon_solid.png" alt="">
                <img src="/images/menu-icon_solid_hover.png" alt="" style="display: none;">
            </a>

            <div class="row">
                <div class="col-xs-3">
                    <img src="/images/saralogo-1.png" alt="">
                </div>

                <div class="col-xs-9">
                    <p class="top-copy">
                        Take a peek at one of the top providers of pulse power solutions, remote sensing&nbsp;&&nbsp;tactical awareness payloads and hardening against EMP threats.
                    </p>
                </div>
            </div>
        </div>

        <img src="/images/transparent-tear-template_vers.png" alt="" class="img-tear">

        <p class="bottom-copy">
            We equip our defenders and providers with breakout technology that works
        </p>

        <div class="clearfix"></div>

        <div class="contact-information-container text-right">
            <a id="image-link-footer" href="/contact"> <img src="/images/mail-icon.png" alt=""></a>
            <br><br>
            <p> 
                <strong>
                    SARA, Inc. <br>
                    <small>
                        Phone: (714) 224-4410 <br>
                        Fax: (714) 224-1710 <br>
                    </small>
                </strong>
            </p>
        </div>
    </div>
@endsection
