@extends('layouts.default', ['navbar' => true, 'pageTitle' => 'Nuclear EMP Hardening Solutions'])

@section('content')
    <div class="page page-nuclear-emp-hardening-solutions">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="heading">Technology</h2>

                    <p>
                        SARA has broad HEMP engineering capabilities for C4I facilities, aircraft and ship.
                    </p>
                </div>

                <div class="col-xs-12 copy-container">
                    <h3 class="heading">Applications</h3>

                    <table class="large-list">
                        @foreach($services as $i => $service)
                            <tr>
                                <td>{{ str_pad($i + 1, 2, '0', STR_PAD_LEFT) }}</td>
                                <td>{{ $service }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>

                <div class="col-xs-12 text-center">
                    <img src="/images/IMG_0583-NO-TEXT.jpg" alt="" class="pull-right content-image">
                </div>

                 <div class="col-xs-12 copy-container">
                    <h3 class="heading">Advantages</h3>

                    <table class="large-list">
                        @foreach($advantages as $i => $advantage)
                            <tr>
                                <td>{{ str_pad($i + 1, 2, '0', STR_PAD_LEFT) }}</td>
                                <td>{{ $advantage }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection
