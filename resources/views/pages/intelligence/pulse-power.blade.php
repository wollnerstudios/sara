@extends('layouts.default', ['navbar' => true, 'pageTitle' => 'Pulse Power Products'])

@section('content')
    <div class="page page-pulse-power">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 copy-container">
                    <h2 class="heading">Technology</h2>

                    <p>SARA Pulsed Power is uniquely equipped and prepared to deliver advanced cutting edge technologies by transforming innovative concepts into tangible, versatile, and practical solutions. Our Pulse Power Group provides unique pulsed power solutions, for each customer from prime power to delivery.</p>

                    <p>Whether the application requires radiated or conducted energy delivery, our team of engineers, physicist, and technicians are able to design and build a state-of-the-art compact solution. We design custom state-of-the-art High Power Microwave (HPM) sources and antennas, compact Marx generators, L-C Oscillators, Pulse Forming Networks & Transmission lines, Pulse Transformers, and Trigger Generators. Every aspect of the solutions we provide can be uniquely optimized providing unparalleled pulsed power system density and performance.</p>

                    <p>In support of our extensive capabilities, SARA has developed several lines of high energy density capacitors as compact energy storage for a variety of pulse power systems from low inductance (supporting rise times of nanoseconds) to high capacitance (storing 100s of kilojoules). Our capacitors are customizable and continuously proven through iterative design and testing to ensure optimum design and maximum performance for each application.</p>

                    <p>Our switch technology enables precision energy delivery, whether the application is suited for precision triggered spark gaps or laser triggered high power semiconductor switches (MOSFETs, SCRs, IGBTs, etc.), supporting repetition rates from 1’s of Hertz to 100’s of kilohertz in the most compact form factor available anywhere. Our high power semiconductor switch assemblies are capable of switching 100’s of kilovolts and 100’s of amps with minimal losses.</p>

                    <p>In addition, SARA utilizes a variety of modeling tools to aid the conceptual and design stages. Advanced full wave electromagnetic and lumped element modeling coupled with our high performance simulation machine provides the platform for robust and advanced solutions to complex requirements.</p>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="copy-container" style="padding-bottom: 0;">
                    <h2 class="heading">Applications</h2>

                    <table class="large-list">
                        @php $index = 0; @endphp
                        @foreach($applications as $application)
                            <tr>
                                <td>{{ str_pad($index + 1, 2, '0', STR_PAD_LEFT) }}</td>
                                <td>{{ $application }}</td>
                            </tr>
                            @php $index++; @endphp
                        @endforeach
                    </table>
                </div>

                <div class="copy-container">
                    <h2 class="heading">Advantages</h2>

                    <table class="large-list">
                        @foreach($advantages as $i => $advantage)
                            <tr>
                                <td>{{ str_pad($i + 1, 2, '0', STR_PAD_LEFT) }}</td>
                                <td>{{ $advantage }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection