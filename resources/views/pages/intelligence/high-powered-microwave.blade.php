@extends('layouts.default', ['navbar' => true, 'pageTitle' => 'High-Powered Microwave Antennas'])

@section('content')
    <div class="page page-high-powered-microwave">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 copy-container">
                    <h2 class="heading">Technology</h2>

                    <p>
                        SARA is uniquely equipped to deliver disruptive and innovative solutions by rapidly transforming nascent technologies into tangible and achievable product designs. In the areas of High-Power Microwave, SARA specializes in numerical and physical modeling, analysis and simulation, prototype fabrication and test, and full subsystem  verification of HPM antenna concepts. SARA has successfully developed and tested numerous designs, including lightweight foldable antennae, steerable and twistable beam apertures, and highly conformable, distributed and low-profile tactical solutions. SARA’s growing family of forward-traveling, fast-wave, leaky-wave HPM antennas demonstrates the tremendous potential and utility of HPM in a variety of industries, including tactical Directed Energy weapons. Law Enforcement, and fossil fuel exploration and extraction.
                    </p>
                </div>
                <div class="col-md-6 col-xs-12">
                    <img src="/images/hpm-antenna-1.png">
                </div>
                <div class="col-md-6 col-xs-12">
                    <img src="/images/hpm-antenna-2.png">
                </div>
                <div style="clear: both;"></div>
                <p style="padding-left: 15px;margin-top: 1rem;">Gigawatt-class FAWSEA antenna: U.S. Patent #7,528,786</p>
                <!-- <div class="col-xs-12 copy-container" style="padding-bottom: 0;">
                    <h2 class="heading">Applications</h2>

                    <table class="large-list">
                        @foreach($applications as $i => $application)
                            <tr>
                                <td>{{ str_pad($i + 1, 2, '0', STR_PAD_LEFT) }}</td>
                                <td>{{ $application }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div> -->

                <div class="col-xs-12 copy-container">
                    <h2 class="heading">Advantages</h2>

                    <table class="large-list">
                        @foreach($advantages as $i => $advantage)
                            <tr>
                                <td>{{ str_pad($i + 1, 2, '0', STR_PAD_LEFT) }}</td>
                                <td>{!! $advantage !!}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
