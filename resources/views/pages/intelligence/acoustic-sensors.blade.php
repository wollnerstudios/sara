@extends('layouts.default', ['navbar' => true, 'pageTitle' => 'Acoustic Sensors&nbsp;for Unmanned Air Vehicles'])

@section('content')
    <div class="page page-acoustic-sensors">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 copy-container">
                    <h2 class="heading">Technology</h2>

                    <p>
                        Unmanned Air Vehicles (UAVs) have become more and more useful for the battlefront as well as the
                        home front in performing reconnaissance and collecting intelligence. To help make them more
                        effective and efficient, SARA pioneered the development of acoustic sensors utilizing Passive
                        Acoustic Non-cooperative Collision-Alert System (PANCAS) technology. PANCAS consists of an array
                        of four lightweight acoustic probes, a custom-designed digital-signal processor, and proprietary
                        windscreen technology and mounts that remove the effects of wind noise and platform vibration.
                    </p>

                    <p>
                        The 3 x 5 inch digital signal processor board performs all acoustic filtering, detection,
                        location, and tracking of targets. The signal processor interfaces with the UAV flight control
                        system to gather GPS location and aircraft attitude data to estimate geographic location of
                        targets. It then sends data directly to the operator’s ground station through the existing UAV
                        downlink combining the data with intelligence information on a geo-registered, interactive
                        mission map to provide battlefield situational awareness. SARA’s Scout UAV Simulation/Ground
                        Station software provides system development, operator training, and mission control/data
                        display tools for the PANCAS user.
                    </p>

                    <img class="content-image" src="/images/acoustic_serv_img1.jpg" alt="" style="margin-top: 3rem;">
                </div>

                <div class="col-xs-12 copy-container" style="padding-bottom: 0;">
                    <h2 class="heading">Applications</h2>

                    <table class="large-list">
                        @foreach($applications as $i => $application)
                            <tr>
                                <td>{{ str_pad($i + 1, 2, '0', STR_PAD_LEFT) }}</td>
                                <td>{{ $application }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>

                <div class="col-sm-9 col-sm-offset-3">
                    <img src="/images/acoustic_serv_img2_cropped.png" alt="acoustic images" class="pull-right content-image">
                    <div class="clearfix"></div>
                </div>

                <div class="col-xs-12 copy-container">
                    <h2 class="heading">Advantages</h2>

                    <table class="large-list">
                        @foreach($advantages as $i => $advantage)
                            <tr>
                                <td>{{ str_pad($i + 1, 2, '0', STR_PAD_LEFT) }}</td>
                                <td>{{ $advantage }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
