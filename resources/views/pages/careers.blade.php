{{-- @extends('layouts.default', ['navbar' => true, 'pageTitle' => 'Careers with SARA in CA or CO']) --}}

@extends('layouts.default', ['navbar' => true, 'pageTitle' => 'Careers'])

@section('content')
    <div class="page page-careers">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 copy-container">
                    <h2 class="heading">Who SARA is looking for:</h2>

                    <p>
                        SARA is looking for dreamers with big ideas as well as doers to build them. We are looking for Swiss Army Knife people who can do a variety of things and support many different assignments, all while having the self-discipline to get the work done right and on time without a lot of handholding. SARA operates in small teams and provides opportunities for independent research in new fields and technology. We are passionate about exploring new technologies and creating new innovations in military systems and commercial products. Whether you’re an industry veteran or new to the field, if you crave the opportunity to develop innovative ideas, create cutting-edge solutions and push the boundaries of what’s possible, SARA might just be the place for you.
                    </p>

                    <p>
                        We provide competitive salaries, a complete benefits package, generous paid time off, and flexible work schedules. SARA, Inc. is an Equal Opportunity, Affirmative Action Employer.
                    </p>
                </div>

                <div class="col-sm-9">
                    <img src="/images/careers-1.jpg" alt="" class="pull-right content-image">
                    <div class="clearfix"></div>
                </div>

                <div class="col-xs-12 copy-container">
                    <h2 class="heading">Some of the exciting careers at SARA include:</h2>

                    <div class="row">
                        <div class="col-md-5">
                            <ul class="careers-list">
                                <li><span>Operations Administrators</span></li>
                                <li><span>Defense Industry Security Officers</span></li>
                                <li><span>Embedded System Developers</span></li>
                                <li><span>Software Developers</span></li>
                                <li><span>Senior Scientists/Senior Engineers</span></li>
                                <li><span>Pulse Power & RF Engineers</span></li>
                                <li><span>EMP/HPM Technologists and Managers</span></li>
                            </ul>
                        </div>
                        <div class="col-md-5">
                            <ul class="careers-list">
                                <li><span>Experimental Physicists</span></li>
                                <li><span>Mechanical Engineers</span></li>
                                <li><span>Electrical Engineers</span></li>
                                <li><span>Signal Processing Analysts</span></li>
                                <li><span>Lab Technicians</span></li>
                                <li><span>Program Managers</span></li>
                            </ul>
                        </div>
                        <div class="col-md-12 col-lg-2">
                            <a href="https://www.speediarms.com/jobboard/default.aspx?customer=bbebc1ae-a3eb-4094-a221-2ead34bcfe3b" class="btn btn-default btn-cta btn-lg careers-link">
                                Open Positions
                            </a>
                        </div>
                    </div>

                </div>

                <div class="col-sm-3">
                    <br>
                </div>

                <div class="col-sm-9 col-sm-offset-3">
                    <img class="content-image" src="/images/careers-2.jpg" alt="">
                    <div class="clearfix"></div>
                </div>

                <div class="col-sm-9 career-copy-container">
                    <h2 class="heading">Contacting Human Resources</h2>

                    <p>
                        Interested applicants should apply for employment through the link above. If you cannot for some reason, you may
                        call (888) 955-7272 for assistance. Please note, no resumes are accepted via email or fax.
                    </p>
                </div>

                <div class="col-sm-9 career-copy-container">
                    <h2 class="heading">Affirmative Action</h2>

                    <p>
                        Scientific Applications & Research Associates, Inc. is an Equal Opportunity Employer. Our Affirmative Action
                        Plan is available for review, by appointment, at the above address, our Cypress, California Corporate Office
                        during normal business hours, Monday through Friday from 8:00 AM to 5:00 PM.
                    </p>
                </div>

                <div class="col-sm-9 career-copy-container">
                    <h2 class="heading">Access Accommodation</h2>

                    <p>
                        If you are a qualified individual with a disability or are a disabled veteran, you have the right to request an
                        accommodation. If you are unable or limited in your ability to use or access our employment pages as a result of
                        your disability you may click here to request an accommodation.
                    </p>
                </div>
            </div>
        </div>

    </div>
@endsection