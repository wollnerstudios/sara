{{-- @extends('layouts.default', ['navbar' => true, 'pageTitle' => 'Contact Information']) --}}
@extends('layouts.default', ['navbar' => true, 'pageTitle' => 'Contact'])

@section('content')
    <div class="page page-contact">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>Office addresses</h1>

                    <h3><b>HEADQUARTERS - CYPRESS, CA</b></h3>

                    <p>
                        SARA Inc.<br>
                        6300 Gateway Drive<br>
                        Cypress, CA, USA, 90630-4844<br>
                        Phone: (714) 224-4410 <br>
                        Fax: (714) 224-1710
                    </p>

                    <h3><b>OTHER LOCATIONS:</b></h3>

                    <p>
                        SARA, Inc. <br>
                        621 S. Sierra Madre Suite 210 <br>
                        Colorado Springs, CO, USA, 80903 <br>
                        Phone: (719) 302-3117
                    </p>

                    <p>
                        SARA, Inc. <br>
                        9700 Research Drive <br>
                        Irvine, CA 92618 <br>
                        Phone: (949) 535-1313
                    </p>
                </div>

                <div class="col-md-6">
                    <form action="/contact" method="post">
                        {{ csrf_field() }}

                        @if(session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        @if($errors->count())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                        </div>

                        <div class="form-group">
                            <label for="company">Company</label>
                            <input type="text" name="company" id="company" class="form-control" placeholder="Company">
                        </div>

                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone">
                        </div>

                        <div class="form-group">
                            <label for="body">Request/Comments:</label>

                            <textarea name="body" id="body" cols="30" rows="6" class="form-control" placeholder="Request/Comments"></textarea>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-default btn-lg btn-cta">
                                Submit
                                <i class="fa fa-caret-right"></i>
                            </button>
                        </div>
                    </form>
                </div>

                <div class="col-xs-12">
                    <br>
                    <br>
                    <br>
                    <br>
                    <img class="content-image" src="/images/offices_contact_page.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
@endsection
