<footer id="footer">
    <p class="text-left">
        <a href="/" class="text-center">
            <img src="/images/logo-footer.png" alt="Sara Logo" class="footer-logo">
            <br>
            <span>Home</span>
        </a>

    </p>
    <p class="text-right">
    	<a id="image-link-footer" href="/contact"> <img src="/images/mail-icon.png" alt=""></a>
        <br>
        <br>
        SARA, Inc. <br>
        <br>
        <small>
            Phone: (714) 224-4410 <br>
            Fax: (714) 224-1710 <br>
        </small>
    </p>
</footer>
