<div id="menu">
    <section class="top-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-2">
                    <a href="/">
                        <img src="/images/sara-logo-menu.png" alt="Sara Logo" class="brand-logo">
                    </a>
                </div>
                <div class="col-xs-12 col-sm-5">
                    <div class="menu-link-list">
                        <h2>Remote Sensing & Tactical Awareness</h2>

                        <a href="" class="menu-link inactive">Tactical Directional Seekers</a>
                        <a href="/technology/acoustic-sensors" class="menu-link">Acoustic Sensors for Unmanned Air Vehicles</a>
                        <a href="" class="menu-link inactive">Hostile Fire Detection Sensors</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5">
                    <div class="clearfix"></div>
                    <div class="menu-link-list">
                        <h2>Pulse Power & Directed Energy Solutions</h2>

                        <a href="" class="menu-link inactive">Integrated Pulsed Power Solutions</a>
                        <a href="/technology/pulse-power" class="menu-link">Pulsed Power Products</a>
                        <a href="/technology/high-powered-microwave" class="menu-link">High Power Microwave Antennas</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bottom-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-5 col-sm-offset-2 col-md-4">
                    <div class="clearfix"></div>
                    <h2>EMP Protection Services</h2>

                    <a href="/technology/nuclear-emp-hardening-solutions" class="menu-link">Nuclear EMP Hardening Solutions</a>
                    <a href="" class="menu-link inactive">Test Instrumentation</a>
                </div>
                <div class="col-sm-5 col-md-2 col-md-offset-1">
                    <h2>About SARA:</h2>

                    <a href="/" class="menu-link">Home</a>
                    <a href="/about" class="menu-link">About</a>
                    <a href="/careers" class="menu-link">Careers</a>
                    <a href="/contact" class="menu-link">Contact</a>
                    {{--<a href="" class="menu-link">SARA rapid prototyping site</a>--}}
                </div>
                <div class="clearfix hidden-md hidden-lg"></div>
                <div class="col-sm-offset-2 col-md-2 col-md-offset-1">
                    <h2 style="visibility: hidden;">About SARA:</h2>
                    <a href="/seaport" class="menu-link" style="padding-left: 0;">SeaPort-e</a>
                </div>
            </div>
        </div>
    </section>

    <a href="#" id="menu-close-button">close</a>
</div>
