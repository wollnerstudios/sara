<nav id="navbar">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-2">
                <a href="/">
                    <img src="/images/saralogo-1.png" alt="Sara Logo" class="navbar-logo">
                </a>
            </div>
            <div class="col-xs-8 text-center">
                <h1 class="page-title">{!! $pageTitle !!}</h1>
            </div>
            <div class="col-xs-2">
                <a href="#" id="menu-button" role="button">
                    <img src="/images/menu-icon_solid.png" alt="">
                    <img src="/images/menu-icon_solid_hover.png" alt="" style="display: none;">
                </a>
            </div>
        </div>
    </div>
</nav>
