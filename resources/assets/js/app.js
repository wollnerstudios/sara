
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$(function () {
    $('#menu-button').click(function () {
        $('#menu').addClass('open');
        $('body').css('overflow', 'hidden');
    });

    $('#menu-close-button').click(function () {
        $('#menu').removeClass('open');
        $('body').css('overflow', 'auto');
    });
});