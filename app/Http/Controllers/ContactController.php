<?php

namespace App\Http\Controllers;

use App\Mail\ContactSubmission;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function submit(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'company' => 'required',
            'phone' => 'required',
            'body' => 'required',
        ]);

        \Mail::to(env('CONTACT_TARGET', 'nickskye7@gmail.com'))->send(new ContactSubmission($request->only(
            'name', 'email', 'company', 'phone', 'body'
        )));

        session()->flash('success', 'Request has been sent.');

        return back();
    }
}
