<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TechnologiesController extends Controller
{
    public function acousticSensors()
    {
        $applications = [
            'Wide area detection of acoustic sources such as heavy vehicles, aircraft, and weapons fire.',
            'Direct UAVs to fly over an acoustic source or cue a gimbaled camera to identify a target.',
            'Detect hostile fire, locate its source, and allow the UAV to be repositioned.',
        ];

        $advantages = [
            'Wide area detection of acoustic sources (heavy vehicles, aircraft, and weapons fire) and can be used to cue the EO/IR camera or the RBCI system.',
            'Monitoring of approach ground or air vehicles and impulsive events (small arms fire, mortars, artillery, explosions) from all directions, day or night, in dust or fog and under camouflage.',
            'Detect operating vehicles or other sound sources, determine their location, and send detection data to the UAV operator to map the battlefield more rapidly and with more confidence than narrow field-of-view imaging sensors allow. ',
            'Lightweight (250 g) and power efficient (consumes 7 watts of 6-volt DC power).',
        ];

        return view('pages.intelligence.acoustic-sensors', compact('applications', 'advantages'));
    }

    public function highPoweredMicrowaves()
    {
        $applications = [
            'Non-lethal ground-based and airborne vehicle stopping',
            'Remote interruption of operation of enemy electronic-bearing systems',
        ];

        $advantages = [
            'Extreme peak power – up to multi-gigawatts',
            'Flat and curved platform-conforming designs',
            'Highly compact: may be install into compact ground and airborne platforms',
            'Highly customizable areas and aspect ratios',
            'Low profiles (as shallow as ~0.43 λ<sub>0</sub>)',
            'High gain & high aperture efficiency',
            'Bandwidth within 10% relative to centerband frequency',
            'Off-broadside beam (scans with frequency)',
            'Proven in lab tests at power levels exceeding one gigawatt',
        ];

        return view('pages.intelligence.high-powered-microwave', compact('applications', 'advantages'));
    }

    public function nuclearEmpHardeningSolutions()
    {
        $services = [
            'MIL-STD-188-125-1,2',
            'MIL-STD-3023', 
            'MIL-STD-4023',
        ];

        $advantages = [
            'HEMP coupling computer modeling',
            'Extensive database of EM coupling data ',
            'Latest hardening techniques and processes',
            'Portable CWI test facility that can be transported to anywhere in the world',
            'Customized CWI instrumentation system developed and manufactured to reduce test time',
            'Wide range of compact HEMP pulsers developed and manufactured',
            'Personnel involved with the development of related military standards',
        ];

        return view('pages.intelligence.nuclear-emp-hardening-solutions', compact('services', 'advantages'));
    }

    public function pulsePower()
    {
        $applications = [
            'Testing EMP/EMI/EMC requires the use of compact pulsers',
            'Pulse Forming Networks',
            'Medical Applications (lithotripsy, cell electroporation), Environmental waste processing (water, emissions, etc.)',
            'Rail guns, lasers, arc welders, semiconductor processing equipment, etc.',
            'HPM Components & Systems (Prime Power, Pulse Modulators, Sources, Antennas, Systems integration & Diagnostics',
            'Low Profile, Conformal, and compact HPM/RF Antennas & Mode Converters',
            'High Power, High-Repetition Rate Solid State Switches',
            'Precision Triggered Vacuum/Gas/Oil Switches and HV Trigger Generators',
            'Electrodynamic Computer Aided Design & Simulation',
        ];

        $advantages = [
            'Customized and optimized for each application, latest in custom and commercially available components',
            'State-of-the-art using the best commercially available components',
            'Reliable and proven through accelerated lifetime and environmental testing',
            'Rapid Prototyping',
        ];

        return view('pages.intelligence.pulse-power', compact('applications', 'advantages'));
    }
}
