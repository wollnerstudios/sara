<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');
Route::get('/about', 'PagesController@about');
Route::get('/careers', 'PagesController@careers');
Route::get('/seaport', 'PagesController@seaport');
Route::get('/contact', 'PagesController@contact');
Route::post('/contact', 'ContactController@submit');

Route::group([
    'prefix' => 'technology'
], function () {
    Route::get('/acoustic-sensors', 'TechnologiesController@acousticSensors');
    Route::get('/high-powered-microwave', 'TechnologiesController@highPoweredMicrowaves');
    Route::get('/nuclear-emp-hardening-solutions', 'TechnologiesController@nuclearEmpHardeningSolutions');
    Route::get('/pulse-power', 'TechnologiesController@pulsePower');
});
